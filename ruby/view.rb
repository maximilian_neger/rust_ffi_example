require 'ffi'

module View
  extend FFI::Library
  ffi_lib "target/release/libexample.dylib"

  attach_function :render_destination_line,
    [:string, :string, :int, :int, :int, :int], :string
end

puts View.render_destination_line('Bulgarien', '1h20m', 25, 12, 75,  213)
