use std::ffi::{CStr, CString};
use std::str;

extern crate libc;
use libc::{uint32_t, c_char};

struct Info<'a> {
  country: &'a str,
  flight_time: &'a str,
  sun: uint32_t,
  water: uint32_t,
  hotel_count: uint32_t,
  min_price: uint32_t,
}

impl<'a> Info<'a> {
  fn new(country: *const c_char,
         flight_time: *const c_char,
         sun: uint32_t,
         water: uint32_t,
         min_price: uint32_t,
         hotel_count: uint32_t)
         -> Self {

    let c_country = unsafe {
      assert!(!country.is_null());
      CStr::from_ptr(country)
    };

    let c_flight_time = unsafe {
      assert!(!flight_time.is_null());
      CStr::from_ptr(flight_time)
    };

    Info {
      country: to_rust_string(c_country),
      flight_time: to_rust_string(c_flight_time),
      sun: sun,
      water: water,
      hotel_count: hotel_count,
      min_price: min_price,
    }
  }

  fn create_info_str(&self) -> String {
    format!("Land: {}, Hotels {}, Flugzeit: {}, Temp. {}°C, Wassertemp. \
              {}°C, Ab {}€",
            self.country,
            self.hotel_count,
            self.flight_time,
            self.sun,
            self.water,
            self.min_price)
  }
}

fn to_rust_string(c_string: &CStr) -> &str {
  match str::from_utf8(c_string.to_bytes()) {
    Ok(value) => value,
    Err(_) => "¯\\_(ツ)_/¯",
  }
}

#[no_mangle]
pub extern "C" fn render_destination_line(country: *const c_char,
                                          flight_time: *const c_char,
                                          sun: uint32_t,
                                          water: uint32_t,
                                          min_price: uint32_t,
                                          hotel_count: uint32_t)
                                          -> *const c_char {
  let info =
    Info::new(country, flight_time, sun, water, min_price, hotel_count);

  match CString::new(info.create_info_str()) {
    Ok(ret) => ret.into_raw(),
    Err(e) => panic!(e),
  }
}
